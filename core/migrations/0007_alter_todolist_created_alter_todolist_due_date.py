# Generated by Django 4.1.3 on 2022-12-09 09:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_alter_todolist_content_alter_todolist_created_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todolist',
            name='created',
            field=models.DateField(default='2022-12-09', verbose_name='Дата начала'),
        ),
        migrations.AlterField(
            model_name='todolist',
            name='due_date',
            field=models.DateField(default='2022-12-09', verbose_name='Дата завершения'),
        ),
    ]
